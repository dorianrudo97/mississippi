import java.util.HashMap;
import java.util.HashSet;

public class Substrings {
    //Print all maximal substrings minimally repeated k times and l long
    public static void printSubstrings(String s, int[] SA, int k, int l) {
        int n = s.length();
        //For each repetition count, save where the first occurrence begins
        HashMap<Integer, HashSet<Integer>> starts = new HashMap<>();
        //For each entry in SA and length, save the first occurrence of the repition and its count
        int[][] minStart = new int[n][2], count = new int[n][2];
        //Set true when one suffix is repeated k times
        boolean kRepeated = true;
        //Iterate over all lengths of substrings as long as there are substrings repeated often enough
        for (int i = 0; i < n && kRepeated; ++i) {
            //it is only kept track of two lengths a time, hence the modulo
            int im = i % 2, im1 = (i + 1) % 2;
            //Iterate over all entries in SA, the current substring is s[SA[j]..SA[j]+i] (-> length i+1)
            for (int j = 0; j < n; ++j) {
                //Reset from previous iteration
                count[j][im] = 0;
                //Out of bounds
                if (SA[j] + i >= n) continue;
                if (j > 0 && (i == 0 || minStart[j][im1] == -1) && SA[j - 1] + i < n && s.charAt(SA[j] + i) == s.charAt(SA[j - 1] + i)) {
                    //The previous suffix' i+1 long prefix is equal to this one's
                    //Repetition count so far is saved, this suffix is receiving cnt+1 as count
                    int cnt = count[j - 1][im];
                    count[j][im] = cnt + 1;
                    //cnt also indicates the first element (not first occurrence in string) in SA starting with this substring
                    // only that one is increased, the other count entries show the distance to the first one
                    ++count[j - cnt][im];
                    //Indicate that this is not the first occurrence
                    minStart[j][im] = -1;
                    //Update the first occurrence of this string if necessary
                    minStart[j - cnt][im] = Math.min(minStart[j - cnt][im], SA[j]);
                } else {
                    //It appears to be the first occurrence of the current substring
                    minStart[j][im] = SA[j];
                    count[j][im] = 1;
                }
            }
            //Check for the substrings with length i if they are maximal and output them
            if (i > 0 && i >= l) {
                kRepeated = false;
                //Save the starting positions of the first occurrences with length i+1
                for (int j = 0; j < n; ++j)
                    if (minStart[j][im] != -1 && count[j][im] >= k) {
                        if (!starts.containsKey(count[j][im]))
                            starts.put(count[j][im], new HashSet<>());
                        starts.get(count[j][im]).add(minStart[j][im]);
                        if (count[j][im] >= k)
                            kRepeated = true;
                    }
                //A substring with length i is now not maximal if it is not suffix or prefix of a i+1 long suffix
                for (int j = 0; j < n; ++j) {
                    HashSet<Integer> st = starts.get(count[j][im1]);
                    if (minStart[j][im1] != -1 && count[j][im1] >= k && (st == null || (!st.contains(minStart[j][im1]) && !st.contains(minStart[j][im1] - 1))))
                        System.out.println(count[j][im1] + "\t" + s.substring(minStart[j][im1], minStart[j][im1] + i));
                }
            }
            starts.clear();
        }
        //Output separately as it is not covered by the previous technique
        if (k == 1)
            System.out.println("1\t" + s);
    }
}
