import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String s;
        int k, l;
        if (args.length != 4) {
            System.out.println("Please supply 4 parameters: input file path, k, l, t (tandem repeats? 1/0)");
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            s = reader.readLine();
        } catch (IOException e) {
            System.out.println("The supplied file either does not exist or cannot be read");
            return;
        }

        try {
            k = Integer.parseInt(args[1]);
            l = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            System.out.println("Please enter numbers for k and l");
            return;
        }

        if (k > s.length() || k < 1 || l > s.length() || l < 1) {
            System.out.println("k and l should be positive integers not larger than the length of the supplied string");
            return;
        }

        if (!args[3].equals("1") && !args[3].equals("0")) {
            System.out.println("Enter either '1' or '0' for t to specify whether tandem repeats should be checked");
            return;
        }

        if (s.length() < 5) {
            System.out.println("Please supply a longer file.");
            return;
        }

        int[] SA = DC3.suffixArray(s);

        SubstringsTandem.printSubstrings(s, SA, k, l, args[3].equals("1"));
    }
}
