import java.util.TreeMap;

public class DC3 {
    //<= for tuples
    private static boolean leq(int a1, int a2, int b1, int b2) {
        return a1 < b1 || (a1 == b1 && a2 <= b2);
    }

    //<= for triples
    private static boolean leq(int a1, int a2, int a3, int b1, int b2, int b3) {
        return a1 < b1 || (a1 == b1 && leq(a2, a3, b2, b3));
    }

    //Gets position of t-th suffix
    private static int getI(int[] SA12, int t, int n0) {
        //Cope with the fact that SA12 is the suffix array of R which is the concatenation of R1 and R2
        //Thus it needs to be checked whether it is a mod 1 or mod 2 suffix
        //The rank is than multiplied by 3 and added to 1 and 2 resp. in order to get a suffix position
        return SA12[t] < n0 ? SA12[t] * 3 + 1 : (SA12[t] - n0) * 3 + 2;
    }

    //Perform radix sort of a into b with keys in k (1..K) and an offset on the key array
    private static void radixSort(int[] a, int[] b, int[] k, int K, int n, int offset) {
        int[] c = new int[K + 1]; //count occurrences of keys
        for (int i = 0; i < n; ++i)
            ++c[k[a[i] + offset]];
        for (int i = 0, sum = 0; i <= K; ++i) {
            int tmp = c[i];
            //Set value in counter to range of the key in the sorted array
            c[i] = sum;
            sum += tmp;
        }
        //Sort values from a into b; counter is incremented for each placed value
        for (int i = 0; i < n; ++i)
            b[c[k[a[i] + offset]]++] = a[i];
    }

    //T, SA need to be 3 larger than n, last 3 should be 0
    private static void suffixSort(int[] T, int[] SA, int n, int K) {
        //Amount of suffixes mod 3 = 0, 1, 2
        int n0 = (n + 2) / 3, n1 = (n + 1) / 3, n2 = n / 3, n02 = n0 + n2;
        //n02 is used in order to have a dummy triplet which is needed for comparison
        int[] SA0 = new int[n0], SA12 = new int[n02 + 3], R = new int[n02 + 3], R0 = new int[n0];

        //Add starting positions of triplets to R, their order does not matter yet for they are to be sorted
        //+(n0-n1) adds the mentioned dummy triplet
        for (int i = 0, j = 0; i < n + (n0 - n1); ++i)
            if (i % 3 != 0)
                R[j++] = i;

        //Do least-significant-bit radix sort on triples
        radixSort(R, SA12, T, K, n02, 2);
        radixSort(SA12, R, T, K, n02, 1);
        radixSort(R, SA12, T, K, n02, 0);

        //Name the triples; same triples get same name
        int name = 0, pr0 = -1, pr1 = -1, pr2 = -1;
        for (int i = 0; i < n02; ++i) {
            if (T[SA12[i]] != pr0 || T[SA12[i] + 1] != pr1 || T[SA12[i] + 2] != pr2) {
                ++name;
                pr0 = T[SA12[i]];
                pr1 = T[SA12[i] + 1];
                pr2 = T[SA12[i] + 2];
            }
            if (SA12[i] % 3 == 1)
                R[SA12[i] / 3] = name; // belongs to R1 -> first half
            else
                R[SA12[i] / 3 + n0] = name; // belongs to R2 -> second half
        }

        //Check if names are unique
        if (name < n02) {
            suffixSort(R, SA12, n02, name);
            //The now unique names are written back to R
            for (int i = 0; i < n02; ++i)
                R[SA12[i]] = i + 1;
        } else { // Names in R are unique, SA12 needs to be constructed
            for (int i = 0; i < n02; ++i)
                SA12[R[i] - 1] = i; //0-based in SA12, 1-based in R
        }

        //Sort mod 0 suffixes with information from SA12
        //R0 are set in the order of SA12
        //Thus, for radixSort is stable, the suffixes are sorted by their first character first
        // and then by the rank of their second character in SA12
        for (int i = 0, j = 0; i < n02; ++i)
            if (SA12[i] < n0) //multiplying by 3 creates mod 0 positions
                R0[j++] = 3 * SA12[i];
        radixSort(R0, SA0, T, K, n0, 0);

        //Merge suffixes from SA0 and SA12
        //p-th suffix from SA0, t-th suffix from SA12 (cope with previous dummy triple), k-th suffix in SA
        for (int p = 0, t = n0 - n1, k = 0; k < n; k++) {
            //first suffix from SA12 and SA0
            int i = getI(SA12, t, n0), j = SA0[p];
            //Different methods are needed to compare SA0 with SA1 and SA2
            if (SA12[t] < n0 ? //is SA1?
                    leq(T[i], R[SA12[t] + n0], T[j], R[j / 3]) :
                    leq(T[i], T[i + 1], R[SA12[t] - n0 + 1], T[j], T[j + 1], R[j / 3 + n0])) {
                //Take suffix from SA12
                SA[k] = i;
                ++t;
                //Check if all SA12 suffixes are used; if so, the remaining SA0 suffixes are appended
                if (t == n02)
                    for (++k; p < n0; ++p, ++k)
                        SA[k] = SA0[p];
            } else {
                //Take suffix from SA0
                SA[k] = j;
                ++p;
                //Check if all SA0 suffixes are used; if so, the remaining SA12 suffixes are appended
                if (p == n0)
                    for (++k; t < n02; ++t, ++k)
                        SA[k] = getI(SA12, t, n0);
            }
        }
    }

    public static int[] suffixArray(String s) {
        int[] SA = new int[s.length() + 3], T = new int[s.length() + 3];

        //Convert string to array of integers; e.g. "azg" -> (1, 3, 2)
        TreeMap<Character, Integer> chars = new TreeMap<>();
        for (int i = 0; i < s.length(); ++i)
            chars.put(s.charAt(i), 0);
        int K = 0;
        for (Character c : chars.navigableKeySet())
            chars.put(c, ++K);
        for (int i = 0; i < s.length(); ++i)
            T[i] = chars.get(s.charAt(i));

        suffixSort(T, SA, s.length(), K);
        return SA;
    }
}
